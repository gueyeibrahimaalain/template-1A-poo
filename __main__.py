from project.view.accueil_view import AccueilView
from project.utilisateur import Utilisateur
from project.rechercher import RechercherAthlete, RechercherPays
from project.filtrer_athlete import FiltrerAthlete
from project.filtrer_pays import FiltrerPays
from project.calculer import Calculer
from project.predire import Predire
view = AccueilView()
chemin = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"

with open("resources/banner.txt", mode="r", encoding="utf-8") as title:
    print(title.read())

while view:
    if isinstance(view, AccueilView):
        view.display_info()
        view = view.make_choice()
    else:
        if isinstance(view, Utilisateur):
            # Demander à l'utilisateur de se connecter
            id_utilisateur = input("id de l'utilisateur : ")
            nom_utilisateur = input("nom de l'utilsateur : ")
            view.se_connecter()
            while view.connecte:

                # Afficher les options avancées
                print("1. Recherche")
                print("2. Filtrage")
                print("3. Affichage des statistiques")
                choix = input("Veuillez sélectionner une option (1/2/3/4) : ")
                if choix == '1':
                    print("1. Rechercher un athlete")
                    print("2. Rechercher un pays")
                    choice = input("Veuillez sélectionner un"
                                   " choix de recherche : ")
                    if choice == '1':
                        athlete = input("Veuillez saisir"
                                        " le nom de l'athlète : ")
                        resultat = RechercherAthlete("//filer-eleves2/id2534/"
                                                     "Downloads/archive/"
                                                     "athlete_events.csv"). \
                            rechercher_athlete(athlete)
                        if resultat is not None:
                            print(resultat)
                        else:
                            print("Aucun résultat pour l'athlète", resultat)
                    elif choice == '2':
                        pays = input("Veuillez entrer le pays :")
                        resultat = RechercherPays(chemin).rechercher_pays(pays)
                        if resultat is not None:
                            print(resultat)
                        else:
                            print("Aucun résultat trouvé pour le pays",
                                  resultat)
                    else:
                        print("Option invalide")
                elif choix == '2':
                    print("1. Filtrer par athlete")
                    print("2. Filtrer par pays")
                    choice = input("Veuillez sélectionner"
                                   " un choix de filtre : ")
                    if choice == '1':
                        medal = input("Veuillez entrer le nom de"
                                      " la médaille : ")
                        nombre_str = input("Veuillez saisir le nombre minimum"
                                           "de médailles : ")
                        try:
                            nombre = int(nombre_str)
                        except ValueError:
                            print("Veuillez saisir un nombre valide")
                        resultat = FiltrerAthlete(chemin). \
                            filtrer_athlete(medal=medal, nombre=nombre)
                        if resultat is not None:
                            print(resultat)
                        else:
                            print("Aucun résultat trouvé", resultat)
                    elif choice == '2':
                        medal = input("Veuillez entrer le nom de"
                                      " la médaille : ")
                        nombre_str = input("Veuillez saisir le nombre minimum"
                                           "de médailles : ")
                        try:
                            nombre = int(nombre_str)
                        except ValueError:
                            print("Veuillez saisir un nombre valide")
                        resultat = FiltrerPays(chemin). \
                            filtrer_pays(medal=medal, nombre=nombre)
                        if resultat is not None:
                            print(resultat)
                        else:
                            print("Aucun résultat trouvé", resultat)
                    else:
                        print('Option invalide')
                elif choix == '3':
                    print("1. Calculs de stats")
                    print("2. Prédiction")
                    choice = input("Veuillez choisir une option de stats : ")
                    if choice == '1':
                        print("1. nb médailles athlete histoire")
                        print("2. nb médailles pays histoire ")
                        print("3. nb médailles athlete par année")
                        print("4. nb médaailles pays par année ")
                        print("5. classement athlete histoire")
                        print("6. classement pays histoire ")
                        sous_choix = input("Veuillez choisir une méthode de"
                                           " calcul : ")
                        if sous_choix == '1':
                            resultat = Calculer(chemin).\
                                calculer_medaille_histoire_athlete()
                            print(resultat)
                        elif sous_choix == '2':
                            resultat = Calculer(chemin).\
                                calculer_medaille_histoire_pays()
                            print(resultat)
                        elif sous_choix == '3':
                            resultat = Calculer(chemin).\
                                calculer_medaille_par_annee_athlete()
                            print(resultat)
                        elif sous_choix == '4':
                            resultat = Calculer(chemin).\
                                calculer_medaille_par_annee_pays()
                            print(resultat)
                        elif sous_choix == '5':
                            resultat = Calculer(chemin).\
                                donner_classement_athlete()
                            print(resultat)
                        elif sous_choix == '6':
                            resultat = Calculer(chemin).\
                                donner_classement_athlete()
                            print(resultat)
                        else:
                            print('Option invalide')
                    elif choice == '2':
                        annee_prediction_str = input("Veuillez entrer l'année"
                                                     " de prédiction : ")
                        try:
                            annee_prediction = int(annee_prediction_str)
                        except ValueError:
                            print("Veuillez saisir une année valide")
                        resultat = Predire(chemin).\
                            predire_nombre_médailles_par_pays(annee_prediction)
                        if resultat is not None:
                            print(resultat)
                elif choix == '4':
                    # Quitter l'application
                    view = None
                else:
                    print("Option invalide. Veuillez sélectionner une option"
                          " valide.")
            else:
                print("La connexion a échoué. Veuillez réessayer.")

with open("resources/exit.txt", mode="r", encoding="utf-8") as exit_message:
    print(exit_message.read())
