from project.view.abstract_view import AbstractView
from project.utilisateur import Utilisateur
from InquirerPy import prompt


class AccueilView(AbstractView):

    def __init__(self):
        super().__init__()
        self.questions = [
            {
                "type": "list",
                "name": "Menu principal",
                "message": "Que souhaitez-vous faire ? ",
                "choices": [
                    "Se connecter",
                    "S'inscrire",
                    "Quitter l'application",
                ],
            }
        ]

    def make_choice(self):
        answers = prompt(self.questions)
        if answers["Menu principal"] == "Se connecter":
            id_user = input("ID de l'utilisateur : ")
            nom_user = input("Nom de l'utilisateur : ")
            try:
                utilisateur = Utilisateur(nom_user, id_user).se_connecter()
                print("Connexion réussie !")
                next_view = utilisateur
            except ValueError as e:
                print(f"Erreur lors de la connexion : {e}")
                next_view = AccueilView()
        elif answers["Menu principal"] == "S'inscrire":
            nom_user = input("Nom de l'utilisateur : ")
            id_user = input("Id de l'utilisateur : ")
            try:
                utilisateur = Utilisateur(nom_user, id_user)
                utilisateur.s_inscrire()
                if utilisateur.connecte:
                    print("Inscription réussie !")
                    next_view = utilisateur
            except ValueError as e:
                print(f"Erreur lors de l'inscription : {e}")
                next_view = AccueilView()
        elif answers["Menu principal"] == "Quitter l'application":
            next_view = None
        else:
            next_view = AccueilView()

        return next_view

    def display_info(self):
        print(" MENU PRINCIPAL ".center(80, "="))
