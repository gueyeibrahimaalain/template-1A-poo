from project.imports import Import
import pandas as pd
from pathlib import Path


class Csvimport(Import):
    """
    Classe pour importer des données à partir d'un fichier CSV.

    Attributes:
        chemin: str
            Le chemin du fichier à importer.
        sep: str
            Le séparateur de champ du fichier CSV (',' par défaut).
        encodage: str
            L'encodage du fichier CSV ('utf_8' par défaut)."""

    def __init__(self, chemin, sep=',', encodage='utf_8'):
        if sep not in [',', ';']:
            raise ValueError("Le format du séparateur n'est pas correct")
        if not isinstance(chemin, str):
            raise TypeError("Le chemin d'accès doit etre une chaine de "
                            "caracteres")
        if not Path(chemin).exists():
            raise FileNotFoundError("Le chemin est introuvable")
        elif not Path(chemin).is_file():
            raise FileNotFoundError("Le chemin ne pointe pas vers un "
                                    "fichier")
        super().__init__(chemin)
        self.sep = sep
        self.encodage = encodage

    def importer(self):
        """
        Importe les données à partir du fichier CSV et les retourne sous
        forme de DataFrame.

        Returns:
            DataFrame: Les données importées à partir du fichier CSV.
        """
        try:
            dataframe = pd.read_csv(self.chemin, sep=self.sep,
                                    encoding=self.encodage)
            return dataframe
        except Exception as e:
            print(f"Erreur lors de l'importation du fichier CSV : {str(e)}")
            return None
