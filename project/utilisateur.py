class Utilisateur:

    """Utilisateurs de l'application.

    Parameters
    ----------
    id_user : str
        identifiant de l'utilisateur

    nom_user: str
        nom de l'utilisateur

    liste_user: dict
        la liste des utilisateurs de l'application caractérisés par leur id et
        leur nom d'utilisateur.

    connecte: bool
        l'état de la connexion. True si l'utilisateur est connecté
    """
    liste_user_inscrits = {}

    def __init__(self, nom_user, id_user):
        if not isinstance(nom_user, str):
            raise TypeError("Le nom d'utlisateur doit etre une chaine"
                            " de caracteres")
        if not id_user.isdigit() or len(id_user) != 6:
            raise TypeError("L'identifiant doit être un digit"
                            " et contenir exactement 6 chiffres.")
        self.nom_user = nom_user
        self.id_user = id_user
        self.connecte = False

    def s_inscrire(self):
        """S'inscrire dans l'application
            L'attribut connecte devient True si la connexion est réussie

            ---------

            Returns:
                None
        """

        if self.nom_user in Utilisateur.liste_user_inscrits.values():
            raise ValueError("Le nom d'utilisateur est deja choisi. Veuillez"
                             " choisir un autre")

        if self.id_user in Utilisateur.liste_user_inscrits.keys():
            raise ValueError("L'identifiant est deja choisi. Veuillez"
                             " choisir un autre")
        else:
            Utilisateur.liste_user_inscrits[self.id_user] = self.nom_user
            self.connecte = True
            print("L'inscription a ete effectuee avec succes")

    def se_connecter(self):
        """Se conneceter à l'application
            L'attribut connecte devient True si la connexion est réussie

            ---------
            Returns:
                None
        """
        if self.id_user not in Utilisateur.liste_user_inscrits.keys():
            raise ValueError("L'identifiant ou le nom d'utilisateur est"
                             " incorect")
        if self.nom_user not in Utilisateur.liste_user_inscrits.values():
            raise ValueError("L'identifiant ou le nom d'utilisateur est"
                             " incorect")
        else:
            self.connecte = True

    def se_deconnecter(self):
        """Se déconnecter de l'application
            L'atrribut devient False si la déconnexion est réussie

            ---------
            Returns:
                None
        """
        self.connecte = False
        print("La deconnexion a ete effectuee avec succes")
