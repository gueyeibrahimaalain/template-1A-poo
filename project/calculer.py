from project.statistiques import Statistiques
import pandas as pd


class Calculer(Statistiques):
    """
    Classe pour effectuer des calculs statistiques sur les données importées.

    Attributes:
        chemin: str
            Le chemin du fichier à importer.
        donnees: DataFrame
            Les données importées sous forme de DataFrame."""
    def __init__(self, chemin):
        super().__init__(chemin)

    def calculer_medaille_par_annee_pays(self, annee):
        """
        Calcule le nombre de médailles par pays pour une année donnée.

        Args:
            annee: int
                L'année pour laquelle effectuer le calcul.

        Returns:
            DataFrame: Le nombre de médailles par pays pour l'année donnée.
        """
        donnees_annee = self.donnees[self.donnees['Year'] == annee]
        medaille_par_pays = donnees_annee.groupby('Team')['Medal'] \
            .value_counts().unstack(fill_value=0)
        return medaille_par_pays

    def calculer_medaille_par_annee_athlete(self, annee):
        """
        Calcule le nombre de médailles par athlète pour une année donnée.

        Args:
            annee: int
                L'année pour laquelle effectuer le calcul.

        Returns:
            DataFrame: Le nombre de médailles par athlète pour l'année donnée.
        """
        if not isinstance(annee, int):
            raise TypeError("L'annee doit etre un entier")
        donnees_athlete = self.donnees[self.donnees['Year'] == annee]
        medaille_par_athlete = donnees_athlete.groupby('Name')['Medal'] \
            .value_counts().unstack(fill_value=0)
        return medaille_par_athlete

    def calculer_medaille_histoire_pays(self):
        """
        Calcule l'historique du nombre de médailles par pays.

        Returns:
            DataFrame: L'historique du nombre de médailles par pays.
        """
        medaille_histoire_pays = self.donnees.groupby(['Team', 'Medal']). \
            size().unstack(fill_value=0)
        return medaille_histoire_pays

    def calculer_medaille_histoire_athlete(self):
        """
        Calcule l'historique du nombre de médailles par athlète.

        Returns:
            DataFrame: L'historique du nombre de médailles par athlète.
        """

        medaille_histoire_athlete = self.donnees. \
            groupby(['Name', 'Medal']).size().unstack(fill_value=0)
        return medaille_histoire_athlete

    def donner_classement_pays(self):
        """
        Donne le classement des pays basé sur le nombre de médailles pondérées.

        Returns:
            Series: Le classement des pays basé sur le nombre de médailles
            pondérées.
        """
        poids_medaille = {'Gold': 5, 'Silver': 3, 'Bronze': 1}
        self.donnees['poids'] = self.donnees['Medal'].map(poids_medaille). \
            fillna(0)
        medailles_par_pays = self.donnees.groupby(['Team', 'Medal']). \
            size().unstack(fill_value=0)
        points_par_pays = medailles_par_pays. \
            multiply(pd.Series(poids_medaille), axis=1)
        classement_pays = points_par_pays.sum(axis=1). \
            sort_values(ascending=False)
        classement_pays['rang'] = classement_pays. \
            rank(ascending=False, method='min')
        return classement_pays

    def donner_classement_athlete(self):
        """
        Donne le classement des athlètes basé sur le nombre de médailles
        pondérées.

        Returns:
            Series: Le classement des athlètes basé sur le nombre de médailles
             pondérées.
        """
        poids_medaille = {'Gold': 5, 'Silver': 3, 'Bronze': 1}
        self.donnees['poids'] = self.donnees['Medal'].map(poids_medaille). \
            fillna(0)
        medailles_par_athlete = self.donnees.groupby(['Name', 'Medal']). \
            size().unstack(fill_value=0)
        points_par_athlete = medailles_par_athlete. \
            multiply(pd.Series(poids_medaille), axis=1)
        classement_athlete = points_par_athlete.sum(axis=1). \
            sort_values(ascending=False)
        classement_athlete['rang'] = classement_athlete. \
            rank(ascending=False, method='min')
        return classement_athlete

    def calculer(self, critere, annee=None):
        """
        Calcule des statistiques en fonction du critère spécifié.
        Args
        ----
        critere: str
            Le critère de calcul ('medaille', 'annee', 'pays').
        annee: int, optional
            L'année pour laquelle effectuer le calcul. Requis si le
            critère est 'annee'.

        Returns:
            DataFrame: Les statistiques calculées en fonction du critère.
        """
        if critere == 'medaille':
            return self.calculer_medaille_histoire_pays()
        elif critere == 'annee':
            if annee is None:
                raise ValueError("L'année est requise pour le calcul "
                                 "par année")
        return self.calculer_medaille_par_annee_pays(annee)

        if critere == 'pays':
            return self.donner_classement_pays
        raise ValueError("Critere invalide")
