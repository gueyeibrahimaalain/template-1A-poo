from abc import ABC, abstractmethod
from pathlib import Path


class Import(ABC):
    """
    Classe abstraite pour l'importation de données à partir d'un fichier.

    Attributes:
        chemin : str
            Le chemin d'accès au fichier à importer.

    """

    def __init__(self, chemin):
        if not isinstance(chemin, str):
            raise TypeError("Le chemin d'accès doit etre une chaine de "
                            "caracteres")
        if not Path(chemin).exists():
            raise FileNotFoundError(f"Le chemin {chemin} est introuvable")
        elif not Path(chemin).is_file():
            raise FileNotFoundError("Le chemin {chemin} ne pointe pas vers un "
                                    "fichier")
        self. chemin = chemin

    @abstractmethod
    def importer(self):
        """
        Méthode abstraite à implémenter dans les sous-classes pour importer
        les données.

        Returns:
            Les données importées à partir du fichier.
        """
        pass
