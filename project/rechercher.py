from project.csvimport import Csvimport


class Rechercher:
    """.

    Parameters
    ----------
    df : df
        la base de données

    """
    def __init__(self, chemin):
        instance_import = Csvimport(chemin, sep=',')
        self.df = instance_import.importer()

    def rechercher(self, critere, valeur):
        """Rechercher sur une colonne dans le dataframe

        Parameters
        ----------
        critere:
            la colonne ou la recherche s'effectue

        valeur:
            l'élément qui est recherché dans la df.

        Return:
            df
            toutes les lignes qui contiennent l'élément recherché
        """

        resultat = self.df[self.df[critere] == valeur]
        return resultat


class RechercherPays(Rechercher):
    """Recherche du palmares d'un pays

    Parameters:
    ----------
    df: dataframe
        la base de données

    """
    def __init__(self, chemin):
        super().__init__(chemin)

    def rechercher_pays(self, nom_pays):
        resultat = self.rechercher('Team', nom_pays)
        return resultat


class RechercherAthlete(Rechercher):
    """Recherche des performances d'un athlete

    Parameters:
    ----------
    df: dataframe
        la base de données

    """
    def __init__(self, chemin):
        super().__init__(chemin)

    def rechercher_athlete(self, nom_athlete):
        resultat = self.rechercher('Name', nom_athlete)
        return resultat
