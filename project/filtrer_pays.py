from project.filtrer import Filtrer


class FiltrerPays(Filtrer):

    def __init__(self, chemin):
        super().__init__(chemin)

    def filtrer_pays(self, medal, nombre):
        return self.filtrer('Team', medal, nombre)
