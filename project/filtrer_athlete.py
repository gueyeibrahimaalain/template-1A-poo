from project.filtrer import Filtrer


class FiltrerAthlete(Filtrer):

    def __init__(self, chemin):
        super().__init__(chemin)

    def filtrer_athlete(self, medal, nombre):
        return self.filtrer('Name', medal=medal, nombre=nombre)
