from abc import ABC, abstractmethod
from project.csvimport import Csvimport


class Statistiques(ABC):
    """
    Classe abstraite pour effectuer des opérations statistiques sur les
    données importées.

    Attributes:
        chemin: str
            Le chemin du fichier à importer.
        donnees : DataFrame
            Les données importées sous forme de DataFrame.
        athletes: Series
              La série contenant les données des athlètes.
        medaille: Series
            La série contenant les données des médailles.
        annee: Series
            La série contenant les données des années.
        pays:Series
            La série contenant les données des pays.

    """

    def __init__(self, chemin):
        importeur = Csvimport(chemin)
        self.donnees = importeur.importer()
        self.athletes = self.donnees['Name']
        self.medaille = self.donnees['Medal']
        self.annee = self.donnees['Year']
        self.pays = self.donnees['Team']

    @abstractmethod
    def calculer(self):
        """
        Méthode abstraite à implémenter dans les sous-classes pour effectuer
        des calculs statistiques.

        Returns:
            None
        """
        pass
