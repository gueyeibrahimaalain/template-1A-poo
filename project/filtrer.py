from project.csvimport import Csvimport


class Filtrer:
    """Filtrer des athlètes ou des pays selon un nombre seuil de médailles
    obtenues

    """
    def __init__(self, chemin):
        instance_import = Csvimport(chemin, sep=',')
        self.donnee = instance_import.importer()

    def filtrer(self, element, medal, nombre):
        """ Fonction permettant de filtrer une colonne(=element) et ainsi
        donner les pays/athlete ayant un nombre de médailles d'un type donné

        Parameters
        ----------
        element: str
            la colonne sur laquelle s'effectue le filtre

        medal: str
            le type de médaille pour lequel on demande le nombre

        nombre: int
            le nombre seuil de médailles

        Returns
            df
            la liste des éléments(pays/athlete) qui remplissent la condition et
            leur nombre de médailles
        """
        data_sans_na = self.donnee.dropna(subset=['Medal'])
        if medal not in ('Gold', 'Silver', 'Bronze', 'Medals'):
            raise ValueError("La médaille doit etre Gold, Silver, Bronze ou"
                             " Medals (pour désigner tous les types)")
        if not isinstance(nombre, int):
            raise TypeError("Le nombre doit etre un entier")
        if medal == 'Medals':
            comptage = data_sans_na[element].value_counts()
            liste_attendue = comptage[comptage >= nombre]
            if liste_attendue.empty:
                print(f"Aucun {element} ne remplit cette condition")
            else:
                return liste_attendue
        else:
            # la liste des éléments qui ont au moins une médaille du type
            #  demandé
            list_medal = data_sans_na[data_sans_na['Medal'] == medal]
            comptage = list_medal[element].value_counts()
            liste_attendue = comptage[comptage >= nombre]
            if liste_attendue.empty:
                print(f"Aucun {element} ne remplit cette condition")
            else:
                return liste_attendue
