from project.statistiques import Statistiques
from project.calculer import Calculer
from sklearn.linear_model import LinearRegression
import numpy as np


class Predire(Statistiques):
    """
    Classe pour prédire le nombre de médailles par pays pour une année donnée.

    Attributes:
        chemin: str
            Le chemin du fichier contenant les données.

    Methods:
        prédire_nombre_médailles_par_pays(annee_prediction): Prédit le nombre
        de médailles par pays pour une année donnée.
    """

    def __init__(self, chemin):
        super().__init__(chemin)
        self.calculer_instance = Calculer(chemin)

    def calculer(self, annee):
        """
        Implémentation de la méthode abstraite calculer() de la classe
        Statistiques.
        Calcule le nombre de médailles par pays pour une année donnée.

        Args:
            annee: int
                L'année pour laquelle effectuer le calcul.

        Returns:
            DataFrame: Le nombre de médailles par pays pour l'année donnée.
        """
        return self.calculer_instance.calculer_medaille_par_annee_pays(annee)

    def predire_nombre_médailles_par_pays(self, annee_prediction):
        """
        Prédit le nombre de médailles par pays pour une année donnée.

        Args:
            annee_prediction: int
                L'année pour laquelle on souhaite faire la prédiction.

        Returns:
            dict: Un dictionnaire contenant les prédictions du nombre
            de médailles par pays pour l'année spécifiée.
        """
        annees_disponibles = self.donnees['Year'].unique()
        if not isinstance(annee_prediction, int):
            raise TypeError("L'année doit être un entier")
        donnees_historiques = {}
        for annee in annees_disponibles:
            donnees_annee = self.calculer(annee)
            if donnees_annee is not None:
                donnees_historiques[annee] = donnees_annee.to_dict()
        predictions = {}
        for annee, medailles_par_pays in donnees_historiques.items():
            if annee >= annee_prediction:
                break
            X = [[annee] for _ in range(len(medailles_par_pays))]
            y = []
            for val in medailles_par_pays.values():
                try:
                    y.append(float(val))
                except (TypeError, ValueError):
                    # Si la valeur ne peut pas être convertie en nombre,
                    #  ignorez-la
                    pass
            if len(X) > 0 and len(y) > 0:
                modele = LinearRegression()
                modele.fit(np.array(X).reshape(-1, 1), y)
                prediction = modele.predict([[annee_prediction]])
                predictions[annee] = prediction
        return predictions
