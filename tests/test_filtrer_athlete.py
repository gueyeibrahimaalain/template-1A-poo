import pytest
from filtrer_athlete import FiltrerAthlete
import pandas as pd


class TestFiltrerAthlete:
    @pytest.fixture
    def filtre_athlete_instance(self):
        chemin = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"
        return FiltrerAthlete(chemin)

    def test_filtrer_athlete(self, filtre_athlete_instance):
        result = filtre_athlete_instance.filtrer_athlete(medal='Gold',
                                                         nombre=1)
        assert isinstance(result, pd.Series)
        assert not result.equals(pd.Series(['Athlete1', 'Athlete4'],
                                 name='Name'))

    def test_filtrer_athlete_medal_invalide(self, filtre_athlete_instance):
        with pytest.raises(ValueError, match="La médaille doit etre Gold,"
                           " Silver, Bronze ou Medals"):
            filtre_athlete_instance.filtrer_athlete(medal='Platinum', nombre=1)

    def test_filtrer_athlete_nombre_non_entier(self, filtre_athlete_instance):
        with pytest.raises(TypeError, match="Le nombre doit etre un entier"):
            filtre_athlete_instance.filtrer_athlete(medal='Gold', nombre='1')
