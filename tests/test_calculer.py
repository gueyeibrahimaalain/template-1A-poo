import pytest
from calculer import Calculer
from statistiques import Statistiques
import re


@pytest.fixture
def calculer_instance():
    chemin = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"
    return Calculer(chemin)


@pytest.mark.parametrize("annee", [2000, 2004, 2008])
def test_calculer_medaille_par_annee_pays(calculer_instance, annee):
    result = calculer_instance.calculer_medaille_par_annee_pays(annee)
    assert result is not None


@pytest.mark.parametrize("annee", [2000, 2004, 2008])
def test_calculer_medaille_par_annee_athlete(calculer_instance, annee):
    result = calculer_instance.calculer_medaille_par_annee_athlete(annee)
    assert result is not None


def test_calculer_medaille_histoire_pays(calculer_instance):
    result = calculer_instance.calculer_medaille_histoire_pays()
    assert result is not None


def test_calculer_medaille_histoire_athlete(calculer_instance):
    result = calculer_instance.calculer_medaille_histoire_athlete()
    assert result is not None


def test_donner_classement_pays(calculer_instance):
    result = calculer_instance.donner_classement_pays()
    assert result is not None


def test_donner_classement_athlete(calculer_instance):
    result = calculer_instance.donner_classement_athlete()
    assert result is not None


class ConcreteStatistiques(Statistiques):
    def calculer(self, critere, annee):
        pass


@pytest.mark.parametrize('critere, annee', [
    ("annee", 2020),
    ("medaille", 2020),
    ("pays", 2020)
])
def test_calculer_critere(critere, annee):
    stats = ConcreteStatistiques("//filer-eleves2/id2534/Downloads/archive/"
                                 "athlete_events.csv")
    result = stats.calculer(critere=critere, annee=annee)
    assert result is None


@pytest.mark.parametrize('annee, erreur, message_erreur', [
    ("2000", TypeError, ("L'annee doit etre un entier")),
    ([2000], TypeError, ("L'annee doit etre un entier")),
])
def test_calculer_annee_fausse(annee, erreur, message_erreur):
    calculer_instance = Calculer("//filer-eleves2/id2534/Downloads/archive/"
                                 "athlete_events.csv")
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        calculer_instance.calculer_medaille_par_annee_athlete(annee)
        calculer_instance.calculer_medaille_par_annee_athlete(annee)


@pytest.mark.parametrize('annee, erreur, message_erreur', [
    (None, ValueError, ("L'année est requise pour le calcul "
                        "par année")),
])
def tester_calculer_annee_vide(annee, erreur, message_erreur):
    calculer_instance = Calculer("//filer-eleves2/id2534/Downloads/archive/"
                                 "athlete_events.csv")
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        calculer_instance.calculer(critere='annee', annee=annee)


@pytest.mark.parametrize('critere, annee, erreur, message_erreur', [
    ([20], 2000, ValueError, ("Critere invalide")),
])
def test_calculer_critere_invalide(critere, annee, erreur, message_erreur):
    calculer_instance = Calculer("//filer-eleves2/id2534/Downloads/archive/"
                                 "athlete_events.csv")
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        calculer_instance.calculer(critere=critere, annee=annee)
