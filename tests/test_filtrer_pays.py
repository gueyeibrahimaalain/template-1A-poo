import pytest
from filtrer_pays import FiltrerPays
import pandas as pd


class TestFiltrerPays:
    @pytest.fixture
    def filtre_pays_instance(self):
        chemin = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"
        return FiltrerPays(chemin)

    def test_filtrer_pays(self, filtre_pays_instance):
        result = filtre_pays_instance.filtrer_athlete(medal='Gold', nombre=1)
        assert isinstance(result, pd.Series)
        assert result.all() == pd.Series(['Country1', 'Country4'],
                                         name='Team').all()

    def test_filtrer_pays_medal_invalide(self, filtre_pays_instance):
        with pytest.raises(ValueError, match="La médaille doit etre Gold,"
                           " Silver, Bronze ou Medals"):
            filtre_pays_instance.filtrer_athlete(medal='Platinum', nombre=1)

    def test_filtrer_pays_nombre_non_entier(self, filtre_pays_instance):
        with pytest.raises(TypeError, match="Le nombre doit etre un entier"):
            filtre_pays_instance.filtrer_athlete(medal='Gold', nombre='1')
