import pytest
from csvimport import Csvimport


def test_import_init():
    import_instance = Csvimport(
     "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"
     )
    assert import_instance.chemin == (
     "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv")

    with pytest.raises(FileNotFoundError):
        Csvimport("/chemin/invalide")

    with pytest.raises(FileNotFoundError):
        Csvimport("/chemin/vers/un/repertoire")

    with pytest.raises(TypeError):
        Csvimport(123)


def test_importer():
    class ConcreteImport(Csvimport):
        def importer(self):
            return "Données importées"
    import_instance = ConcreteImport(
        "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv")
    assert import_instance.importer() == "Données importées"


def test_import_class():
    test_import_init()
    test_importer()
