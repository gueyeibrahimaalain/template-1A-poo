import pytest
from rechercher import RechercherPays, RechercherAthlete


@pytest.fixture
def chemin_fichier_csv():
    return "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"


def test_rechercher_pays(chemin_fichier_csv):
    rechercher_pays = RechercherPays(chemin_fichier_csv)
    resultat = rechercher_pays.rechercher_pays('France')
    assert len(resultat) > 0
    print(resultat)


def test_rechercher_athlete(chemin_fichier_csv):
    rechercher_athlete = RechercherAthlete(chemin_fichier_csv)
    resultat = rechercher_athlete.rechercher_athlete('Michael Phelps')
    assert len(resultat) == 0
    print(resultat)
