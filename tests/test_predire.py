import pytest
from predire import Predire
import re


class FakeCalculer:
    def __init__(self, chemin):
        pass

    def calculer_medaille_par_annee_pays(self):
        return {
            2000: {'Country1': 10, 'Country2': 20},
            2004: {'Country1': 15, 'Country2': 25},
            2008: {'Country1': 20, 'Country2': 30}
        }


@pytest.fixture
def predire_instance():
    chemin = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"
    return Predire(chemin)


@pytest.mark.parametrize("annee_prediction", [100, 102, 105])
def test_predire_nombre_medailles_par_pays(predire_instance, annee_prediction):
    predire_instance.calculer_instance = FakeCalculer(chemin="dummy_path")

    result = predire_instance. \
        predire_nombre_médailles_par_pays(annee_prediction)

    assert result is not None
    assert isinstance(result, dict)
    assert len(result) == 0
    assert annee_prediction not in result.keys()


def test_calculer(predire_instance):
    predire_instance.calculer_instance = FakeCalculer(chemin="dummy_path")
    result = predire_instance.calculer()

    expected_result = {
        2000: {'Country1': 10, 'Country2': 20},
        2004: {'Country1': 15, 'Country2': 25},
        2008: {'Country1': 20, 'Country2': 30}
    }

    assert result == expected_result


@pytest.mark.parametrize('annee_prediction, erreur, message_erreur', [
         ("2000", TypeError, ("L'année doit être un entier")),
         ([2000], TypeError, ("L'année doit être un entier")),
])
def test_annee_prediction_incorrect(annee_prediction, erreur, message_erreur):
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        predire_instance = Predire(chemin="//filer-eleves2/id2534/Downloads/"
                                          "archive/athlete_events.csv")
        predire_instance.predire_nombre_médailles_par_pays(annee_prediction)
