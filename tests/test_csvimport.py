import pytest
from csvimport import Csvimport
import re


@pytest.mark.parametrize("sep, chemin, encodage, erreur, message_erreur", [
    (',', '//filer-eleves2/id2534/Downloads/archive/', 'utf_8',
     FileNotFoundError, ("Le chemin ne pointe pas vers un "
                         "fichier")),
    (';', '/chemin/invalide/fichier.csv', 'utf_8', FileNotFoundError, (
        "Le chemin est introuvable")),
    ('|', '//filer-eleves2/id2534/Downloads/archive/athlete_events.csv',
     'utf_8', ValueError, ("Le format du séparateur n'est pas correct")),
    (';', ['chemin'],
     'utf_8', TypeError, ("Le chemin d'accès doit etre une chaine de "
                          "caracteres")),
])
def test_csvimport_incorrect(sep, chemin, encodage, erreur, message_erreur):
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        csv_importer = Csvimport(chemin, sep=sep, encodage=encodage)
        csv_importer.importer()


def test_csvimport_fichier_existant():
    cheminok = "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv"

    csv_importer = Csvimport(cheminok)
    dataframe = csv_importer.importer()
    assert dataframe is not None


def test_csvimport_importation_echec():
    csv_importer = Csvimport(chemin="//filer-eleves2/id2534/Downloads/"
                                    "fmexsf.pdf")
    dataframe = csv_importer.importer()
    assert dataframe is None
