import pytest
from statistiques import Statistiques
import pandas as pd


class FakeStatistiques(Statistiques):
    def __init__(self, chemin):
        super().__init__(chemin)

    def calculer(self):
        pass


@pytest.mark.parametrize("chemin", [
    "//filer-eleves2/id2534/Downloads/archive/athlete_events.csv",
])
def test_statistiques_init(chemin):
    statistiques_instance = FakeStatistiques(chemin)
    assert statistiques_instance.donnees is not None
    assert isinstance(statistiques_instance.donnees, pd.DataFrame)
    assert statistiques_instance.athletes is not None
    assert isinstance(statistiques_instance.athletes, pd.Series)
    assert statistiques_instance.medaille is not None
    assert isinstance(statistiques_instance.medaille, pd.Series)
    assert statistiques_instance.annee is not None
    assert isinstance(statistiques_instance.annee, pd.Series)
    assert statistiques_instance.pays is not None
    assert isinstance(statistiques_instance.pays, pd.Series)


@pytest.mark.parametrize("chemin, expected_error", [
    ("invalid_path.csv", FileNotFoundError),
    ("",
     FileNotFoundError)
])
def test_statistiques_init_error(chemin, expected_error):
    chemin = "//filer-eleves2/id2534/Downloads/archive"
    with pytest.raises(expected_error):
        FakeStatistiques(chemin)
