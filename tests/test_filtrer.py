import pytest
from filtrer import Filtrer
import re


@pytest.mark.parametrize('element, medal, nombre, erreur, message_erreur', [
    ('Team', 'Plantinium', 20, ValueError, ("La médaille doit etre Gold,"
     " Silver, Bronze ou Medals (pour désigner tous les types)")),
    ('Team', 'Bronze', [20], TypeError, ("Le nombre doit etre un entier")),
])
def test_init_echec(element, medal, nombre, erreur, message_erreur):
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        filtrer_instance = Filtrer("//filer-eleves2/id2534/Downloads/"
                                   "archive/athlete_events.csv")
        filtrer_instance.filtrer(element, medal, nombre)


@pytest.fixture
def medaille():
    return "Medals"


def test_filtrer_medals(medaille):
    filtrer_instance = Filtrer("//filer-eleves2/id2534/Downloads/"
                               "archive/athlete_events.csv")
    filtrer_instance.filtrer('Team', medaille, 5)


def test_filtrer_liste_team_vide(medaille):
    filtrer_instance = Filtrer("//filer-eleves2/id2534/Downloads/"
                               "archive/athlete_events.csv")
    res = filtrer_instance.filtrer('Team', medaille, 200000000000000)
    assert res is None


def test_filtrer_liste_athlete_vide(medaille):
    filtrer_instance = Filtrer("//filer-eleves2/id2534/Downloads/"
                               "archive/athlete_events.csv")
    res = filtrer_instance.filtrer('Name', medaille, 800000)
    assert res is None


@pytest.fixture
def medal1():
    return "Gold"


def test_filtrer_liste_athlete_vide1(medal1):
    filtrer_instance = Filtrer("//filer-eleves2/id2534/Downloads/"
                               "archive/athlete_events.csv")
    res = filtrer_instance.filtrer('Name', medal1, 800000)
    assert res is None
