import pytest
from utilisateur import Utilisateur


@pytest.mark.parametrize(
    "nom_user, id_user, expected",
    [
        ("Alice", "123456", False),  # Nom et identifiant valides
        (123, "123456", True),        # Nom invalide
        ("Bob", "12345a", True),      # Identifiant invalide
    ],
)
def test_creation_utilisateur(nom_user, id_user, expected):
    if expected:
        with pytest.raises(TypeError):
            Utilisateur(nom_user, id_user)
    else:
        user = Utilisateur(nom_user, id_user)
        assert user.nom_user == nom_user
        assert user.id_user == id_user
        assert not user.connecte


@pytest.mark.parametrize(
    "nom_user, id_user, expected_exception",
    [
        ("Bob", "123456", None),    # Inscription réussie
        ("Bob", "789012", ValueError),  # Nom utilisateur déjà utilisé
        ("Charlie", "123456", ValueError),  # Identifiant déjà utilisé
    ],
)
def test_inscription(nom_user, id_user, expected_exception):
    user = Utilisateur(nom_user, id_user)
    if expected_exception:
        with pytest.raises(expected_exception):
            user.s_inscrire()
    else:
        user.s_inscrire()
        assert user.connecte
        assert id_user in Utilisateur.liste_user_inscrits
        assert Utilisateur.liste_user_inscrits[id_user] == nom_user


@pytest.mark.parametrize(
    "nom_user, id_user, existant, expected_exception",
    [
        ("Alice", "123456", True, None),   # Connexion réussie
        ("Bob", "123456", False, ValueError),  # Identifiant invalide
        ("Alice", "789012", False, ValueError),  # Nom utilisateur invalide
    ],
)
def test_connexion(nom_user, id_user, existant, expected_exception):
    user = Utilisateur(nom_user, id_user)
    if existant:
        Utilisateur.liste_user_inscrits[id_user] = nom_user
    if expected_exception is not None:
        with pytest.raises(expected_exception):
            user.se_connecter()
    else:
        user.se_connecter()
        assert user.connecte is True


def test_deconnexion():
    user = Utilisateur("Alice", "123456")
    user.connecte = True
    user.se_deconnecter()
    assert not user.connecte
